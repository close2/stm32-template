#include <stm32f1xx_hal.h>

#include "astm32_clock.hpp"
#include "astm32_gpio.hpp"
#include "astm32_tim.hpp"

#define N_TASKS 4
#define N_QUEUES 4
#define N_SEMAPHORES 4
#define N_EVENTS 4

#include "cocoOS.hpp"

typedef astm32::Line<astm32::Port::B, GPIO_PIN_7> B7;
typedef astm32::Pwm<B7> pwm;

void initAll(void)
{

    HAL_Init();

    astm32::SystemClock::init();

    B7::init(GPIO_MODE_AF_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_LOW, true);

    pwm::init((uint16_t)(HAL_RCC_GetPCLK1Freq() / 10000), 10000);
    pwm::setPulse(9000);

    cocoOS::init();
}

static void task(void)
{
    task_open();
    for (;;) {
        pwm::setPulse(1000);
        task_wait(5000);
        pwm::setPulse(9000);
        task_wait(5000);
    }
    task_close();
}

int main(void)
{
    initAll();

    task_create(task, NULL, 1, NULL, 0, 0);

    cocoOS::start();

    return 0;
}
