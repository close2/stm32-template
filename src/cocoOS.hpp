#pragma once

#include <stm32f1xx_hal.h>

#include <cocoos.h>

extern "C" void SysTick_Handler(void)
{
    HAL_IncTick();
    HAL_SYSTICK_IRQHandler();
}

extern "C" void HAL_SYSTICK_Callback(void)
{
    os_tick();
}

namespace cocoOS
{
void init()
{
    os_init();
}

// won't return
void start()
{
    os_start();
}
} // namespace cocoOS
