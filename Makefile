# -----------------------------------------------------------------------------
# CMake project wrapper Makefile ----------------------------------------------
# -----------------------------------------------------------------------------

SHELL       := /bin/bash
RM          := rm -rf
MKDIR       := mkdir -p
BUILDDIR    := build
GDB         := arm-none-eabi-gdb
APP_ELF     := $(BUILDDIR)/app.elf

all: $(BUILDDIR)/Makefile
	@ $(MAKE) -C build

./build/Makefile:
	@  ($(MKDIR) build > /dev/null)
	@  (cd build > /dev/null 2>&1 && cmake ..)

distclean:
	@  ($(MKDIR) build > /dev/null)
	@  (cd build > /dev/null 2>&1 && cmake .. > /dev/null 2>&1)
	@- $(MAKE) --silent -C build clean || true
	@- $(RM) ./build/Makefile
	@- $(RM) ./build/src
	@- $(RM) ./build/test
	@- $(RM) ./build/CMake*
	@- $(RM) ./build/cmake.*
	@- $(RM) ./build/*.cmake
	@- $(RM) ./build/*.txt

flash: $(APP_ELF)
	$(GDB) -x gdb.bmp.conf -ex "set confirm off" -ex "quit" $(APP_ELF)

cmake-debug:
	cmake -DCMAKE_BUILD_TYPE=Debug -Bbuild .

cmake-release:
	cmake -DCMAKE_BUILD_TYPE=Release -Bbuild .


ifeq ($(findstring distclean,$(MAKECMDGOALS)),)
	$(MAKECMDGOALS): $(BUILDDIR)/Makefile
	@ $(MAKE) -C build $(MAKECMDGOALS)
endif

